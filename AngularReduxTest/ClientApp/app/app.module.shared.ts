import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { RouterModule } from '@angular/router';

import { AppComponent } from './components/app/app.component';
import { NavMenuComponent } from './components/navmenu/navmenu.component';
import { HomeComponent } from './components/home/home.component';
import { FetchDataComponent } from './components/fetchdata/fetchdata.component';
import { CounterComponent } from './components/counter/counter.component';

import { StoreModule } from "@ngrx/store";
import { metaReducer, reducerToken, reducerProvider } from "./store/index";
import { EffectsModule } from "@ngrx/effects";
import { FetchDataActionCreator } from "./components/fetchdata/fetchdata.actioncreator";
import { FetchDataService } from "./components/fetchdata/fetchdata.service";

@NgModule({
    declarations: [
        AppComponent,
        NavMenuComponent,
        CounterComponent,
        FetchDataComponent,
        HomeComponent
    ],
    imports: [
        CommonModule,
        HttpModule,
        FormsModule,
        RouterModule.forRoot([
            { path: '', redirectTo: 'home', pathMatch: 'full' },
            { path: 'home', component: HomeComponent },
            { path: 'counter', component: CounterComponent },
            { path: 'fetch-data', component: FetchDataComponent },
            { path: '**', redirectTo: 'home' }
        ]),
        /*
           Provide the application reducer to the store.
       */
        //StoreModule.forRoot(metaReducer),
        StoreModule.forRoot(reducerToken),
        EffectsModule.forRoot([FetchDataActionCreator])
    ],
    providers: [
        FetchDataActionCreator,
      FetchDataService,
        reducerProvider
    ]
})
export class AppModuleShared {
}
