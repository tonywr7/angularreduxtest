﻿/*
  Import createSelector from reselect to make selection of different parts of the state fast efficient
 */
import { createSelector } from '@ngrx/store';
/*
  Import the store logger to log all the actions to the console
 */
import { storeLogger } from "ngrx-store-logger";

import * as counterReducer from "../components/counter/counter.reducer";
import * as fetchDataReducer from "../components/fetchdata/fetchdata.reducer";

import { compose } from "@ngrx/store";
import { combineReducers, State, ActionReducerMap } from "@ngrx/store";
import { state, InjectionToken } from "@angular/core";

export interface AppState {
    counter: counterReducer.State
    fetchdata: fetchDataReducer.WeatherForecastsState
}

export const reducers = {
    counter: counterReducer.reducer,
    fetchdata: fetchDataReducer.reducer
};


const developmentReducer: Function = compose(storeLogger(), combineReducers)(reducers);


export function metaReducer(state: any, action: any) {
    return developmentReducer(state, action);
}

export const reducerToken = new InjectionToken<ActionReducerMap<AppState>>("Reducers");
export const reducerProvider = [{ provide: reducerToken, useFactory: developmentReducer }];

/* counter */
export const getCounterState = (state: AppState) => state.counter;
export const getCounter = createSelector(getCounterState, counterReducer.getCounter);

/* fetch data */
export const getFetchDataState = (state: AppState) => state.fetchdata;
export const getWeatherForecasts = createSelector(getFetchDataState, fetchDataReducer.getWeatherForecasts);
export const getForecastsLoaded = createSelector(getFetchDataState, fetchDataReducer.getIsLoaded);

