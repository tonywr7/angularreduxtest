﻿import { Action } from '@ngrx/store';

export const INCREMENT_COUNT = 'INCREMENT_COUNT';
export const DECREMENT_COUNT = 'DECREMENT_COUNT';

export class IncrementCountAction implements Action {
    readonly type = INCREMENT_COUNT;

    constructor(public incrementBy: number) {
    }
}

export class DecrementCountAction implements Action {
    readonly type = DECREMENT_COUNT;

    constructor(public decrementBy: number) {
    }
}

export type CounterActions = IncrementCountAction | DecrementCountAction;