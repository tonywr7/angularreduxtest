﻿import * as counterActions from './counter.actions';

export interface State {
    count: number;
}

const initialState: State = {
    count: 0
}

/*
  The reducer of the counter state. Each time an action for the counter is dispatched,
  it will create a new state for the counter.
 */

export function reducer(state = initialState, action: counterActions.CounterActions): State {
    switch (action.type) {
        case counterActions.INCREMENT_COUNT: {
            return Object.assign({}, state, {
                count: state.count + action.incrementBy
            });
        }
        case counterActions.DECREMENT_COUNT: {
            return Object.assign({}, state, {
                count: state.count - action.decrementBy
            });
        }
        default:
            return state;
    }
}

export const getCounter = (state: State) => state.count;
