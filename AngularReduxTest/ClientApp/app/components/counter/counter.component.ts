import { Component, OnInit } from '@angular/core';
import { Store } from "@ngrx/store";
import { Observable } from "rxjs";
/**
 * Import the root state in order to select parts of it.
 */
import * as fromRoot from '../../store/index';
/*
 * Import the counter actions to make dispatching from the component possible.
 */
import * as counterActions from './counter.actions';

@Component({
    selector: 'counter',
    templateUrl: './counter.component.html'
})
export class CounterComponent {
    //public currentCount = 0;
    public currentCount$: Observable<number>;

    constructor(private store: Store<fromRoot.AppState>) {
        this.currentCount$ = store.select(fromRoot.getCounter);
    }

    public incrementCounter() {
        //this.currentCount++;
        this.store.dispatch(new counterActions.IncrementCountAction(1));
    }

    public decrementCounter() {
        this.store.dispatch(new counterActions.DecrementCountAction(1));
    }

}
