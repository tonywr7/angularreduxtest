﻿import { Action } from '@ngrx/store';
import { WeatherForecast } from "./weatherforecast.model";

export const REQUEST_WEATHER_FORECASTS = 'REQUEST_WEATHER_FORECASTS';
export const RECEIVE_WEATHER_FORECASTS_SUCCESS = 'RECEIVE_WEATHER_FORECASTS_SUCCESS';
export const RECEIVE_WEATHER_FORECASTS_FAILED = 'RECEIVE_WEATHER_FORECASTS_FAILED';

export class RequestWeatherForecastsAction implements Action {
    readonly type = REQUEST_WEATHER_FORECASTS;
}

export class ReceiveWeatherForecastsSuccessAction implements Action {
    readonly type = RECEIVE_WEATHER_FORECASTS_SUCCESS;
    constructor(public forecasts: WeatherForecast[]) { }
}

export class ReceiveWeatherForecastsFailedAction implements Action {
    readonly type = RECEIVE_WEATHER_FORECASTS_FAILED;
}

export type FetchDataActions = RequestWeatherForecastsAction | ReceiveWeatherForecastsSuccessAction | ReceiveWeatherForecastsFailedAction;