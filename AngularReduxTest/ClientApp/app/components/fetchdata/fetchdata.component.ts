import { Component, Inject } from '@angular/core';
import { Store } from "@ngrx/store";
import { Observable } from "rxjs";
/**
 * Import the root state in order to select parts of it.
 */
import * as fromRoot from '../../store/index';
import * as fetchDataActions from './fetchdata.actions';

@Component({
    selector: 'fetchdata',
    templateUrl: './fetchdata.component.html'
})
export class FetchDataComponent {

    public forecasts: WeatherForecast[];
    public forecasts$: Observable<WeatherForecast[]>;
    public forecastsLoaded$: Observable<boolean>;

    //constructor(http: Http, @Inject('BASE_URL') baseUrl: string) {
    //    http.get(baseUrl + 'api/SampleData/WeatherForecasts').subscribe(result => {
    //        this.forecasts = result.json() as WeatherForecast[];
    //    }, error => console.error(error));
    //}

    constructor(private store: Store<fromRoot.AppState>) {
        this.forecasts$ = store.select(fromRoot.getWeatherForecasts);
        this.forecastsLoaded$ = store.select(fromRoot.getForecastsLoaded);
    }

    ngOnInit() {

        this.forecastsLoaded$.subscribe((isLoaded: boolean) => {
            if (!isLoaded)
            {
                this.store.dispatch(new fetchDataActions.RequestWeatherForecastsAction());
            }
        });

    }

}



interface WeatherForecast {
    dateFormatted: string;
    temperatureC: number;
    temperatureF: number;
    summary: string;
}
