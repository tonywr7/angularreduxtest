﻿import * as fetchdataActions from './fetchdata.actions';
import { WeatherForecast } from "./weatherforecast.model";

export interface WeatherForecastsState {
    isLoaded: boolean;
    forecasts: WeatherForecast[];
}

const initialState: WeatherForecastsState = {
    isLoaded: false,
    forecasts: []
}

/*
  The reducer of the counter state. Each time an action for the counter is dispatched,
  it will create a new state for the counter.
 */

export function reducer(state = initialState, action: fetchdataActions.FetchDataActions): WeatherForecastsState {
    switch (action.type) {
        case fetchdataActions.REQUEST_WEATHER_FORECASTS: {
            return Object.assign({}, state, {
                forecasts: [],
                isLoaded: false
            });
        }
        case fetchdataActions.RECEIVE_WEATHER_FORECASTS_SUCCESS: {
            return Object.assign({}, state, {
                forecasts: action.forecasts,
                isLoaded: true
            });
        }
        case fetchdataActions.RECEIVE_WEATHER_FORECASTS_FAILED: {
            return Object.assign({}, state, {
                forecasts: [],
                isLoaded: false
            });
        }
        default:
            return state;
    }
}

export const getIsLoaded = (state: WeatherForecastsState) => state.isLoaded;
export const getWeatherForecasts = (state: WeatherForecastsState) => state.forecasts;
