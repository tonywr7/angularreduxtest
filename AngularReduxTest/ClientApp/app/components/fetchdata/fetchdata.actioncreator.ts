﻿import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/switchMap';
import { Observable } from 'rxjs/Observable';
import { Injectable } from "@angular/core";
import * as fetchdataActions from "./fetchdata.actions";
import { Actions, Effect } from "@ngrx/effects";
import { FetchDataService } from "./fetchdata.service";


@Injectable()
export class FetchDataActionCreator {

    constructor(private actions: Actions, private fetchDataService: FetchDataService) { }

    @Effect() fetchWeatherForecasts$ = this.actions.ofType(fetchdataActions.REQUEST_WEATHER_FORECASTS)
            .switchMap(() => this.fetchDataService.getWeatherForecasts()
                .map((response: any) => {
                return new fetchdataActions.ReceiveWeatherForecastsSuccessAction(response.json());
            }))
            .catch((response: any) => Observable.of(new fetchdataActions.ReceiveWeatherForecastsFailedAction()));

}
